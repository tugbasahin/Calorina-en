﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Speech;
using KacKalori3.Droid.Dependency;
using Android.Gms.Ads;
using Android.Gms.Common;
using Firebase.Messaging;
using Firebase.Iid;
using Android.Util;
using Lottie.Forms.Droid;
using Java.Lang;

namespace KacKalori3.Droid
{
    [Activity(Label = "Calorina", Icon = "@drawable/calorina", Theme = "@style/MyTheme.Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private  int SPLASH_DISPLAY_TIME = 3000;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
           
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            // Name of the MainActivity theme you had there before.
            // Or you can use global::Android.Resource.Style.ThemeHoloLight
            base.SetTheme(Resource.Style.MyTheme);

            base.OnCreate(bundle);

            MobileAds.Initialize(ApplicationContext, "ca-app-pub-1028791551070626~5646034696");

            global::Xamarin.Forms.Forms.Init(this, bundle);

           // AnimationViewRenderer.Init();

            LoadApplication(new App());
        }
		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
            const int VOICE = 10;
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == VOICE)
            {
                if (resultCode == Result.Ok)
                {
                    var matches = data.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
                    if (matches.Count != 0)
                    {
                        var textInput = matches[0];
                        if (textInput.Length > 500)
                            textInput = textInput.Substring(0, 500);
                        SpeechToText.SpeechText = textInput;
                    }
                }
                SpeechToText.autoEvent.Set();
            }
		}
    
	}
}
