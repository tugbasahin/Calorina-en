﻿using System;
using System.Threading.Tasks;
using SQLite.Net;

namespace KacKalori3.Dependency
{
    public interface ISpeechToText
    {
        Task<string> SpeechToTextAsync();
        SQLiteConnection CreateConnection();
    }
}
