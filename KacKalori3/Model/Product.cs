﻿using System;
namespace KacKalori3.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Food{ get; set;}
        public int Water { get; set; }
        public int Energy_Kcal { get; set; }
        public int Protein { get; set; }
        public int Lipd_Total { get; set; }
        public int Carbohydrate { get; set; }
        public int Sugar_Total { get; set; }
        public int Calcium { get; set; }
        public int Iron { get; set; }
        public int Magnesium { get; set; }
        public int Phosphor { get; set; }
        public int Potassium { get; set; }
        public int Sodium { get; set; }
        public int Zinc { get; set; }
        public int Selenium { get; set; }
        public int Vit_C { get; set; }
        public int Vit_B6 { get; set; }
        public int Vit_B12 { get; set; }
        public int Vit_A_UI { get; set; }
        public int Vit_A_RAE { get; set; }
        public int Vit_E { get; set; }
        public int Vit_K { get; set; }
        public int Chlstrl { get; set; }
        public int GmWt { get; set; }
        public string G_Desc { get; set; }

    }
}
