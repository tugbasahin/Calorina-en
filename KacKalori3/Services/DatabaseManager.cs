﻿using System;
using System.Collections.Generic;
using KacKalori3.Dependency;
using KacKalori3.Model;
using SQLite.Net;
using Xamarin.Forms;

namespace KacKalori3.Services
{
    public class DatabaseManager:IDisposable
    {
        SQLiteConnection dbConnection;

        public DatabaseManager()
        {
            dbConnection = DependencyService.Get<ISpeechToText>().CreateConnection();
        }

        public List<Product> GetProduct(string name)
        {
            if (name!="")
            {
                try
                {
                    var food = dbConnection.Query<Product>($"Select * From Product where Food like '%{name}%'");
                    if (food != null)
                    {
                        return food;
                    }
                }
                catch (Exception ex)
                {

                }
              
            }
          
            return null;
        }

        public int SaveProduct(Product aProduct)

        {
            return dbConnection.Insert(aProduct);
        }
        public  List<Product> GetAllProduct()
        {
            var product = dbConnection.Query<Product>($"Select * From Product");

            dbConnection.Close();
            //Query<Product>($"select * from table where name like '%{name}'");

            return product;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbConnection.Close();
                dbConnection.Dispose();
            }

            dbConnection = null;
        }

        ~DatabaseManager()
        {
            Dispose(false);
        }
    }
}
