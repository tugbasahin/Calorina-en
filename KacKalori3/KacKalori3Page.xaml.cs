﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KacKalori3.Dependency;
using KacKalori3.Model;
using KacKalori3.Services;
using KacKalori3.ViewModel;
using Xamarin.Forms;


namespace KacKalori3
{
    public partial class KacKalori3Page : ContentPage
    {
        ObservableCollection<Product> list;
        DatabaseManager dbm = new DatabaseManager();

        List<Product> listProduct = new List<Product>();

        bool isListLoad;

        void KacKalori3Page_Appearing(object sender, EventArgs e)
        {
            lvFood.SelectedItem = null;
        }


        public KacKalori3Page()
        {
            InitializeComponent();
            Appearing += KacKalori3Page_Appearing;

            if (!isListLoad)
            {
                listProduct = dbm.GetAllProduct();

                list = new ObservableCollection<Product>(listProduct);

                lvFood.ItemsSource = list;

                isListLoad = true;
            }
        }



        public async void lvFood_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            
            var theSelectedFood = lvFood.SelectedItem as Product;

            try
            {
                if (theSelectedFood != null)
                {
                    var detailPage = new FoodDetailPage(theSelectedFood);
                    detailPage.Title = "100 gr " + theSelectedFood.Food;

                    await Navigation.PushAsync(detailPage);
                    ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.FromHex("#60D5E2");
                    ((NavigationPage)Application.Current.MainPage).Style = Style.BasedOn;
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        async void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            try
            {
                if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected)
                {

                    lvFood.ItemsSource = null;

                    var speechText = await WaitForSpeechToText();
                    lblFood.Text = string.IsNullOrEmpty(speechText) ? "Nothing Recorded" : speechText;

                    if (speechText != null)
                    {
                        var prd = list.Where(X => X.Food.Contains(speechText.ToUpper()));

                        if (prd != null)
                        {
                            lvFood.ItemsSource = prd;
                        }
                    }
                }
                else
                {
                    await DisplayAlert("Internet connection", "Please check the internet connection", "Cancel");
                }

            }
            catch (System.Exception ex)
            {

            }
        }

        async void onSearch_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected)
            {
                lvFood.BeginRefresh();

                if (string.IsNullOrWhiteSpace(e.NewTextValue))

                    lvFood.ItemsSource = list;
                else
                    lvFood.ItemsSource = list.Where(a => a.Food.Contains(e.NewTextValue.ToUpper()));

                lvFood.EndRefresh();
            }
            else
            {
                await DisplayAlert("Internet connection", "Please check the internet connection", "Cancel");
            }
        }

        async Task<string> WaitForSpeechToText()
        {
            return await DependencyService.Get<ISpeechToText>().SpeechToTextAsync();
        }

    }
}
