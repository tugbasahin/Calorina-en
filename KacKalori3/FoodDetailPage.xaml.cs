﻿using System;
using System.Collections.Generic;
using KacKalori3.Model;
using Xamarin.Forms;

namespace KacKalori3
{
    public partial class FoodDetailPage : ContentPage
    {
        public FoodDetailPage()
        {
            InitializeComponent();

        }
        public FoodDetailPage(Product product)
        {
            InitializeComponent();

            lblMeasure.Text = "100 gr";
            lblEnergy_Kcal.Text = product.Energy_Kcal.ToString() +" kcal";
            lblProtein.Text = product.Protein.ToString()+" g";
            lblWater.Text = product.Water.ToString() +" g";
            lblCarbohydrate.Text = product.Carbohydrate.ToString()+ " g";
            lblLipd_Total.Text = product.Lipd_Total.ToString()+" g";
            lblSugar_Total.Text = product.Sugar_Total.ToString()+ " g";
            lblCalcium.Text = product.Calcium.ToString()+ " mg";
            lblIron.Text = product.Iron.ToString()+ " mg";
            lblMagnesium.Text = product.Magnesium.ToString()+ " mg";
            lblPhosphor.Text = product.Phosphor.ToString()+ " mg";
            lblPotassium.Text = product.Potassium.ToString()+ " mg";
            lblSodium.Text = product.Sodium.ToString()+ " mg";
            lblZinc.Text = product.Zinc.ToString()+ " mg";
            lblSelenium.Text = product.Selenium.ToString()+ " mcg";
            lblVit_C.Text = product.Vit_C.ToString()+ " mg";
            lblVit_B6.Text = product.Vit_B6.ToString()+ " mg";
            lblVit_B12.Text = product.Vit_B12.ToString()+ " mcg";
            lblVit_A_UI.Text = product.Vit_A_UI.ToString()+ " IU";
            //lblVit_A_RAE.Text = product.Vit_A_RAE.ToString();
            lblVit_E.Text = product.Vit_E.ToString()+ " mg";
            lblVit_K.Text = product.Vit_K.ToString()+ " mcg";
            lblChlstrl.Text = product.Chlstrl.ToString()+ " mg";

        
        }
    }
}
